<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GProwl {

	public function __construct(){
		$this->ci =& get_instance();
		$this->ci->load->config('gprowl', TRUE);

		if((!function_exists('curl_init')) && ($this->ci->config->item('debug', 'gprowl'))){
			echo "CURL NO CARGADO";
			}
	}



    public function mensaje($titulo = false, $mensaje = false, $prioridad = false, $url = false){
    	if($mensaje and $titulo){
    		$usuarios = $this->ci->config->item('usuarios', 'gprowl');
    		$url_prowl = $this->ci->config->item('url_prowl', 'gprowl');
    		$aplicacion = $this->ci->config->item('application', 'gprowl');

    		$prioridad = (int)$prioridad;

    		if($prioridad > 2){ $prioridad = 2; }
    		if($prioridad < -2){ $prioridad = -2; }
    		//echo (int)$prioridad;
    		$destino = '';
    		foreach($usuarios as $user){
    			$destino = $destino.$user.",";
    			}
    			$peticion = 'apikey='.$destino.'&priority='.$prioridad.'&event='.$titulo.'&description='.$mensaje.'&application='.$aplicacion;
    			if($url){
    				$peticion=$peticion.'&url='.$url;
    				}

    			
    			$process = curl_init($url_prowl);
				curl_setopt ($process, CURLOPT_POST, true); 
				curl_setopt ($process, CURLOPT_POSTFIELDS, $peticion);
			    curl_setopt ($process, CURLOPT_TIMEOUT, 60);
				curl_setopt ($process, CURLOPT_HEADER, false); 
			    curl_setopt ($process, CURLOPT_RETURNTRANSFER, TRUE);
			    curl_setopt ($process, CURLOPT_SSL_VERIFYPEER, false);

			    $response = curl_exec($process);
				curl_close($process);

				if($response){
					return true;
					}
					else{ 
						return false;
						}
				
    			
    		}
    }
}
/* End of file GProwl.php */
?>
