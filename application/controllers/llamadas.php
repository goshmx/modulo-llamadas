<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';

/**
 * Class Llamadas
 */
class Llamadas extends REST_Controller{

	# Variables globales de estado y mensajes de operacion
	public $error = array('estado'=>'error','mensaje'=>'Uno o mas datos son requeridos');
	public $operacion = array();

	# Constructor
    function __construct(){
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
	}

	function llamada_get(){
		$telefono = $this->input->get('telefono');
        if($telefono){
            $result = $this->webservice($telefono);
            $resultData = get_object_vars($result);
            $this->response(array("datos"=>$resultData));
        }
        else{
            $this->response(array('status' => false ,'msg'=>'Telefono requerido'),200);
        }
	}

    function webservice($telefono){
	    ini_set('soap.wsdl_cache_enabled',0);
	    ini_set('soap.wsdl_cache_ttl',0);
	    libxml_disable_entity_loader(false);

	    $wsdl           = getenv('WSDL');
	    $endpoint       = getenv('WSDL_ENDPOINT');
	    $certificate    = 'cert/'.getenv('WSDL_CERTIFICATE');
	    $password       = getenv('WSDL_PASSWORD');

	    $options = array(
	        'location'      => $endpoint,
	        'keep_alive'    => true,
	        'trace'         => true,
	        'local_cert'    => $certificate,
	        'passphrase'    => $password,
	        'cache_wsdl'    => WSDL_CACHE_NONE,
		'stream_context' => stream_context_create(
        		array(
            			'ssl' => array(
                			'verify_peer'       => false,
                			'verify_peer_name'  => false,
            			)
        		)
    		)
	    );

	    try {
	        $soapClient = new SoapClient($wsdl, $options);
	        $params = array(
	            'id_tel'    => $telefono,
	            'token'     => getenv('SOAP_TOKEN')
	        );
	        $data = $soapClient->solicitaRegistro($params);
	        return $data->solicitaRegistroReturn;
	    } catch(Exception $e) {
	        return $e->getMessage();
	    }
	}

}
