## Variables de entorno

Definir en el sistema operativo las siguientes variables de entorno con los datos requeridos, *estos datos han sido proporcionados por la dependencia*
```
ENVIRONMENT_URL=http://localhost/ #URL sobre la que se ejecutará el servicio
WSDL=https://<ip>/<categoria>/<servicio>?wsdl #URL de consulta del webservice
WSDL_ENDPOINT=https://<ip>/<categoria>/<servicio>?wsdl #URL de consulta del webservice
WSDL_CERTIFICATE=certifcado.pem #Nombre de certificado
WSDL_PASSWORD=contraseña #Contraseña asignada por la dependencia
SOAP_TOKEN=<token> #Token asignago por la dependencia
```
**Archivos requeridos**

Sino existe la carpeta **cert** en la raíz del proyecto, crearlo e incluir dentro el **certificado.pem**, este archivo también es proporcionado por la dependencia


**¿Cómo crear variables de entorno?**

[Windows](https://medium.com/@jonDotsoy/crear-nueva-variable-de-entorno-para-el-comando-php-u-otro-software-81a8604537ed#.sfs56z1ia)

[Linux/Unix](http://www.sysadmit.com/2016/04/linux-variables-de-entorno-permanentes.html)

## Métodos

[GET] /llamadas/llamada/?telefono=

Resultado de la consulta en formato **JSON**

```json
{
  "datos": {
    "calle": null,
    "ciudad_localidad": null,
    "colonia": null,
    "cp": null,
    "del_mun": null,
    "eme_pos": null,
    "entidad": null,
    "fecha_registro": null,
    "id_tel": null,
    "msid": null,
    "no_ext": null,
    "no_int": null,
    "time": null,
    "x": null,
    "y": null
  }
}
```